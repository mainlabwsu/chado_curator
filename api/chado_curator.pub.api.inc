<?php

use Drupal\Component\Utility\Xss;
use Drupal\user\Entity\User;
use Drupal\file\Entity\File;
use Drupal\Core\File\FileSystemInterface;

/**
 * Configurables
 */

// available status
function chado_curator_pub_conf_status() {
    return array(
        0 => 'na',
        1 => 'Ready to check',
        2 => 'Data checked',
        3 => 'Loaded to Chado',
        4 => 'Data published',
        5 => 'Data requested'
    );
}

// curate options
function chado_curator_pub_conf_curate() {
    return array(
        1 => 'Yes',
        2 => 'No',
        3 => 'Future',
    );
}

// curate options
function chado_curator_pub_conf_future() {
    return array(
        1 => 'GWAS',
        2 => 'Expression',
    );
}

// Limit of items per page
function chado_curator_pub_conf_page_limit() {
    return \Drupal::state()->get('chado_curator_pub_page_limit', 10);
}

// Limit of files to upload
function chado_curator_pub_conf_upload_limit() {
    return \Drupal::state()->get('chado_curator_pub_upload_limit', 5);
}

// Fast forward pages
function chado_curator_pub_conf_fforward_pages() {
    return \Drupal::state()->get('chado_curator_pub_fforward_pages', 50);
}

// Pub ID limit
function chado_curator_pub_conf_pub_id_limit() {
    return \Drupal::state()->get('chado_curator_pub_pub_id_limit', 0);
}


/**
 * Get pub list
 */
function chado_curator_pub_get_pub_list($limit = NULL, $page = 0, $assignment = 0, $scope = NULL, $count = FALSE, $order = 'ASC', $pub_id = NULL, $pub_id_op = '>=', $title = NULL, $ignore_pid_cutoff = FALSE) {
    $orderby = ''; 
    if (!$count) {
      if ($order == 'DESC' || $order == 'ASC') {
        $orderby = " ORDER BY P.pub_id $order";
      } else if ($order == 'TITLE_ASC') {
        $orderby = " ORDER BY P.title";
      } else if ($order == 'TITLE_DESC') {
        $orderby = " ORDER BY P.title DESC";
      }
    }
    $select = "
        P.pub_id AS pid,
        P.title,
        (SELECT string_agg(name, ' ') FROM chado.db WHERE db_id IN (SELECT db_id FROM chado.dbxref WHERE dbxref_id IN (SELECT dbxref_id FROM chado.pub_dbxref WHERE pub_id = P.pub_id))) AS db,
        CP.*
    ";
    if ($count) {
        $select = ' COUNT (*) ';
    }
    $sql_assignment = '';
    if ($assignment) {
        if ($assignment == 'any') {
            $sql_assignment = " (CP.assigned_uid IS NOT NULL AND CP.assigned_uid != 0)";
        }
        else {
            $sql_assignment = " CP.assigned_uid = $assignment";
        }
    } else {
        $sql_assignment = ' (CP.assigned_uid IS NULL OR CP.assigned_uid = 0)';
    }
    $sql = "
      SELECT
        $select
      FROM chado.pub P
      LEFT JOIN chado_curator_pub CP ON P.pub_id = CP.pub_id
      WHERE 1 = 1
      ";
    if (!$ignore_pid_cutoff) {
        $sql .= " AND $sql_assignment";
    }

    $sql_limit = '';
    if ($limit) {
        $offset = $limit * $page;
        $sql_limit = " LIMIT $limit OFFSET $offset";
    }
    $sql_scope = '';
    if ($scope) {
        if ($scope == 'WORK') {
            $sql_scope = ' (CP.curate NOT IN (2, 3) OR CP.curate IS NULL) AND (CP.delete != 1 OR CP.delete IS NULL) AND (CP.status != 4 OR CP.status IS NULL)';
        } else if ($scope == 'READY') {
            $sql_scope = ' CP.status = 1';
        }  else if ($scope == 'CHECKED') {
            $sql_scope = ' CP.status = 2';
        } else if ($scope == 'LOADED') {
            $sql_scope = ' CP.status = 3';
        } else if ($scope == 'PUBLISHED') {
            $sql_scope = ' CP.status = 4';
        } else if ($scope == 'DELETE') {
            $sql_scope = ' CP.delete = 1';
        } else if ($scope == 'CURATE' || $scope == 'CURATEINC') {
            $sql_scope = ' CP.curate = 1';
        } else if ($scope == 'FUTURE') {
            $sql_scope = 'CP.curate = 3';
        }else {
            $scope = NULL;
        }
    }
    $result = NULL;
    if ($pub_id) {
        $sql .= " AND P.pub_id $pub_id_op $pub_id";
    }
    if ($title) {
        $contains_word = str_replace("'", "''", $title);
        $sql .= " AND lower(P.title) LIKE lower('%" . $contains_word. "%')";
    }
    if ($scope && !$ignore_pid_cutoff) {
        $sql .= " AND " . $sql_scope;
    }
    $pub_id_limit = chado_curator_pub_conf_pub_id_limit();
    if ($pub_id_limit > 0 && !$ignore_pid_cutoff && $scope != 'CURATEINC') {
        $sql .= " AND P.pub_id >= $pub_id_limit";
    }
    $sql .= $orderby . $sql_limit;
    $result = \Drupal::database()->query($sql);
    if ($count) {
        return $result->fetchAll();
    }
    else {
        $publist = array();
        while ($p = $result->fetchObject()) {
            $publist [$p->pid] = $p;
        }
        return $publist;
    }
}

function chado_curator_pub_insert_or_update_pub ($pub) {
    $user = \Drupal::currentUser();
    $uid = $user->id();
    $time = time();
    // required values
    $pub_id = $pub['pub_id'];
    $assigned_uid = $pub['assigned_uid'];

    // Handle file deletion
    $del_files = $pub['detail']['files'];
    foreach ($del_files AS $did => $del) {
        if (isset($del['file_delete']) && $del['file_delete'] == 1) {
            $dfile = File::load($did);
            if (is_object($dfile)) {
                \Drupal::service('file.usage')->delete($dfile, 'chado_curator', NULL, NULL, 0);
                $dfile->delete();
            }
        }
    }

    // Handle file upload
    $existing_path = \Drupal::database()->query("SELECT file_path FROM chado_curator_pub WHERE pub_id = :pub_id", array(':pub_id' => $pub_id))->fetchField();
    $fids = array();
    if ($existing_path) {
        $existing_file_path = json_decode($existing_path, TRUE);
        
        if (is_array($existing_file_path)) {
            foreach ($existing_file_path AS $fid) {
                if ($fid) {
                    $fids [] = $fid;
                }
            }
        }
    }
    $files = $_FILES;
    foreach ($files AS $element_id => $ufile) {
        if (preg_match('/chado_curator-' . $pub_id . '-file_path_\d+/', $element_id)) {
            $fname = $ufile['name'];
            $tmp_path = $ufile['tmp_name'];
            $directory = 'public://chado_curator_pub';
            $file_system = \Drupal::service('file_system');
            $file_system->prepareDirectory($directory, FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
            $realfile = $file_system->copy($tmp_path, $directory . '/' . $fname, FileSystemInterface::EXISTS_RENAME);
            $file = File::create([
                'filename' => $fname,
                'uri' => $realfile,
                'status' => 1,
                'uid' => 1,
            ]);
            $file->save();
            $file_usage = \Drupal::service('file.usage');
            $file_usage->add($file, 'chado_curator', 'pub', $pub_id);
            $fid = \Drupal::database()->query("SELECT fid FROM file_managed WHERE uri = :uri", [':uri' => $realfile])->fetchField();
            $fids[] = $fid;
        }
    }
    $file_path = json_encode($fids);

    //optional values
    $delete = isset($pub['delete']) ? $pub['delete'] : 0;
    $curate = isset($pub['curate']) ? $pub['curate'] : 0;
    $status = isset($pub['status']) ? $pub['status'] : 0;
    $comments = isset($pub['detail']['comments']) ? $pub['detail']['comments'] : '';
    $comments = str_replace("'", "''", $comments);
    $history = array();
    $future = isset($pub['detail']['future']) ? $pub['detail']['future'] : '';
    $fstore = array();
    foreach ($future AS $k => $v) {
        if ($v != 0) {
            $fstore [] = $k;
        }
    }
    $future = json_encode($fstore);

    // check if record exists. if yes, update. otherwise, insert.
    $sql = 'SELECT * FROM chado_curator_pub WHERE pub_id = :pub_id';
    $exists = \Drupal::database()->query($sql, array(':pub_id' => $pub_id))->fetchObject();
    if(isset($exists->pub_id)) {
        $history = $exists->history ? json_decode($exists->history, TRUE) : [];
        if ($status != $exists->status) {
            $all_status = chado_curator_pub_conf_status();
            if ($all_status[$status] == 'na') {
                $history [] = date("Y-m-d h:i", $time) . ' Reset status (' . $user->getAccountName() . ')';
            }
            else {
                $history [] = date("Y-m-d h:i", $time) . ' ' . $all_status[$status] . ' (' . $user->getAccountName() . ')';
            }
        }
        $histore = json_encode($history);
        /*
        $sql = "
        UPDATE chado_curator_pub SET
          pub_id = $pub_id,
          delete = $delete,
          curate = $curate,
          assigned_uid = $assigned_uid,
          file_path = '$file_path',
          status = $status,
          comments = '$comments',
          future = '$future',
          history = '$histore',
          ";
        $sql .= "
          timestamp = $time
        WHERE pub_id = $pub_id
        ";
        \Drupal::database()->query($sql);
        */
        $connection = \Drupal::service('database');
        $result = 
        $connection->update('chado_curator_pub')
            ->fields([
                'pub_id' => $pub_id,
                'delete' => $delete,
                'curate' => $curate,
                'assigned_uid' => $assigned_uid,
                'file_path' => $file_path,
                'status' => $status,
                'comments' => $comments,
                'future' => $future,
                'history' => $histore,
            ])
            ->condition('pub_id', $pub_id, '=')
            ->execute();
    }
    else {
        if ($status) {
            $all_status = chado_curator_pub_conf_status();
            $history [] = date("Y-m-d h:i", $time) . ' ' . $all_status[$status] . ' (' . $user->getAccountName() . ')';
        }
        $histore = json_encode($history);
        /*
        $sql = "
        INSERT INTO chado_curator_pub (
          pub_id,
          delete,
          curate,
          assigned_uid,
          file_path,
          status,
          comments,
          future,
          history,
          timestamp)
        VALUES (
          $pub_id,
          $delete,
          $curate,
          $assigned_uid,
          '$file_path',
          $status,
          '$comments',
          '$future',
          '$histore',
          $time
        )
        ";
        \Drupal::database()->query($sql);
        */
        $connection = \Drupal::service('database');
        $result = 
        $connection->insert('chado_curator_pub')
            ->fields([
                'pub_id' => $pub_id,
                'delete' => $delete,
                'curate' => $curate,
                'assigned_uid' => $assigned_uid,
                'file_path' => $file_path,
                'status' => $status,
                'comments' => $comments,
                'future' => $future,
                'history' => $histore,
                'timestamp' => $time
            ])
            ->execute();

    }
}

/**
 * Get users that are allowed to 'curate chado pubs'
 */
function chado_curator_get_curators ($unassigned = '(unassigned)', $anyone = '(anyone)') {
    $curators = ['0' => $unassigned];
    if ($anyone) {
        $curators['any'] = '(anyone)';
    }
    $roles = \Drupal\user\Entity\Role::loadMultiple();
    foreach ($roles AS $r) {
        if ($r->hasPermission('curate chado pubs')) {
            $ids = 
            \Drupal::entityQuery('user')
            ->accessCheck(FALSE)
            ->condition('status', 1)
            ->condition('roles', $r->id())
            ->execute();
            $users = User::loadMultiple($ids);
            foreach($users as $user){
                $username = $user->get('name')->value;
                $uid = $user->get('uid')->value;
                $curators [$uid] = $username;
            }
        }
    }
    return $curators;
}

/**
 * Get associated features for a pub
 */
function chado_curator_get_features($pub_id) {
    $sql = "
      SELECT
        type_id,
        (SELECT name FROM chado.cvterm WHERE cvterm_id = F.type_id) AS type,
        count(*) AS count
      FROM chado.feature F
      WHERE feature_id IN
      (SELECT feature_id FROM chado.feature_pub
      WHERE pub_id = :pub_id)
      GROUP BY F.type_id
    ";
    $results = \Drupal::database()->query($sql, array(':pub_id' => $pub_id));
    $feature = array();
    $counter = 0;
    while ($r = $results->fetchObject()) {
        $feature[$r->type] = $r->count;
        $counter ++;
    }
    $num_maps = \Drupal::database()->query("SELECT count(*) FROM chado.featuremap_pub WHERE pub_id = :pub_id", array('pub_id' => $pub_id))->fetchField();
    $counter += $num_maps;
    $summary = "<div class=\"chado_curator_pub_summary_no_results\">No associated features</div>";
    if ($counter > 0) {
        $summary = "<ul class=\"chado_curator_pub_summary_items\">";
        foreach ($feature AS $k => $v) {
            $summary .= "<li> $k : $v </li>";
        }
        if ($num_maps > 0) {
            $summary .= "<li> map : $num_maps </li>";
        }
        $summary .= "</ul>";
    }
    return $summary;
}

/**
 * Get associated features for a pub
 */
function chado_curator_get_duplicates($pub_id) {
    $sql = "
      SELECT
        pub_id,
        title
      FROM chado.pub
      WHERE
        regexp_replace(lower(title), '[ ,()*&^%$#@!~.:\"''-_+=/|<>?]', '', 'g') =
        (SELECT regexp_replace(lower(title), '[ ,()*&^%$#@!~.:\"''-_+=/|<>?]', '', 'g') FROM chado.pub WHERE pub_id = :pub_id)
      AND pub_id != :pub_id2
    ";
    $results = \Drupal::database()->query($sql, array(':pub_id' => $pub_id, ':pub_id2' => $pub_id));
    $pubs = array();
    $counter = 0;
    while ($r = $results->fetchObject()) {
        $pubs[$r->pub_id] = $r->title;
        $counter ++;
    }
    $dup = "<div class=\"chado_curator_pub_summary_no_results\">No duplicate found</div>";
    if ($counter > 0) {
        $dup = "<ul class=\"chado_curator_pub_summary_items\">";
        foreach ($pubs AS $k => $v) {
            $link = chado_display_get_path() . '/pub/' . $k;
            $value = $link ? '<a href="' . $link . '">' . $v . '</a>' : $v;
            $dup .= "<li>[Pub ID: $k] $value</li>";
        }
        $dup .= "</ul>";
    }
    return $dup;
}

function chado_curator_get_history($histore) {
    $history = $histore ? json_decode($histore, TRUE) : [];
    $his = "<div class=\"chado_curator_pub_summary_no_results\">No status history</div>";
    if (is_array($history) && count($history) > 0) {
        $his = "<ul class=\"chado_curator_pub_summary_items\">";
        foreach ($history AS $v) {
            $his .= "<li>$v</li>";
        }
        $his .= "</ul>";
    }
    return $his;
}

/**
 * Email API for sending mails to the curators
 */
function chado_curator_email_curator($uid, $subject, $message) {
    $sendmail_from = '';
    $site_mail = \Drupal::state()->get('site_mail', $sendmail_from);
    if (empty($site_mail) || $site_mail == $sendmail_from) {
        \Drupal::messenger()->addError('You should create an administrator mail address for your site! <a href="admin/config/site-information">Do it here</a>.', 'error');
    }
    // send the email
    $account = user_load($uid);
    $language = user_preferred_language($account);
    $params = array(
        'account' => $account,
        'subject' => $subject,
        'message' => $message,
    );
    if ($account->uid == 0) {
        $message = 'Anonymous user; no message sent.';
        \Drupal::messenger()->addMessage($message);
    }
    else {
        $sent = drupal_mail('chado_curator', 'notify', $account->mail, user_preferred_language($account), $params, $site_mail);
        if (!empty($sent['result'])) {
            \Drupal::messenger()->addMessage('message was sent to ' . $account->name);
            $message = Xss::filter(nl2br($sent['body']), array('br', 'a')); // Return sanitized e-mail with HTML breaks added.
        }
        else {
            $message = 'There was a problem sending the message to ' . $account->name;
            \Drupal::messenger()->addError($message);
        }
    }
}

function chado_curator_mail($key, &$message, $params) {
    $data['user'] = $params['account'];
    $options['language'] = $message['language'];
    $message['subject'] = $params['subject'];
    $message['body'][] = $params['message'];
}

function chado_curator_pub_set_file_permanent ($fid) {
  $file = File::load($fid);
  $file->setPermanent();
  $file->save();
  $user = \Drupal::currentUser();
  \Drupal::service('file.usage')->add($file, 'chado_curator', 'pub', $user->id());
}