# Chado Curator

Chado Curator is a tool for curating data stored in a Tripal/Chado database.

Currently, Chado curator supports only curation of the publication data. It is especially useful when the Tripal (https://github.com/tripal/tripal)
pub importer is configured to import pubs recurrently from NCBI PubMed and/or USDA National Agriculture Library (AGRICOLA). Curators can easily find
newly imported pubs by setting a pub_id cutoff to show only pubs that are newer than this limit. These pubs can then be curated manually to ensure
the quality of the automatically imported data.

The pub curator allows a curator to assign pubs to other curators with permission. Once log in, the curator is presented with pubs he or she is assigned.
For unassigned pubs, the pub curator implements a set of filters to help find the pubs of interests easily. By selecting a category under the 'Scope',
pubs can be filtered by flag or status. A default 'Working list' filter is pre-selected to list pubs that are not flagged as 'No curate', 'Future curate',
or 'Delete'. Once the curation status is changed to 'Data published', the pub will be excluded from the working list as well.

A Search Pub form is also available if the curator wants to find a specific pub. It can be used to search all pubs stored in Chado, regardless of the
pub_id cutoff setting, filters, or curator assignement.

The pub curator also allows commenting and uploading files related to a pub. It also keeps track of the curation status in history. If a pub is associated
with the Chado feature or featuremap, the data will be summarized under the 'Final Data Summary' section. For removing redundancy, the pub curator also
suggests 'Possible Duplicates' by comparing the pub title with other existing pubs and hinted duplication when two pubs have a similar title.

Chado Curator is created by Main Bioinformatics Lab (Main Lab) at Washington State University. Information about the Main Lab can be found at: https://www.bioinfo.wsu.edu

## Screenshot
![Image](screenshot.jpg "screenshot")

## Requirement
 - Drupal 10.x or 11.x

## Version
4.3.0

## Download
The Chado Curator module can be downloaded from GitLab:

https://gitlab.com/mainlabwsu/chado_curator

## Installation

1. Download 'chado_curator':

    ```
    git clone https://gitlab.com/mainlabwsu/chado_curator
    ```

2. Enable the 'chado_curator' module:

    Enable it from the Drupal Extend adminitrative interface [https://your.site/admin/modules]

    or by using the 'drush' command:

    ```
    drush pm-enable chado_curator
    ```

3. To change the settings for chado_curator, use the URL below:

    Go to: https://your.site/admin/config/mainlab/chado_curator

4. To start using chado_curator:

    Go to: https://your.site/chado_curator/pub

## Administration (Configuration > Mainlab > Chado Curator)

  The following are the options you can change from the administrative interface:

 - Page limit:
   The number of rows to show on each page.

 - Upload limit:
   The number of files allowed to be uploaded for each pub.

 - Fast Forward Pages:
   The number of pages to fast forward when clicking on the >> button.

 - Pub ID cutoff:
   Show only pubs with pub_id greater than or equal to this number. Pubs older than this limit will be hidden from display.

## Problems/Suggestions
Tripal MegaSearch module is still under active development. For questions or bug
report, please contact the developers at the Main Bioinformatics Lab by emailing to:
dev@bioinfo.wsu.edu