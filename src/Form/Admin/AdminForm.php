<?php
namespace Drupal\chado_curator\Form\Admin;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class AdminForm extends FormBase {

    /**
     * {@inheritdoc}
     */
     public function getFormId() {
        return 'chado_curator_admin_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state) {
        $form['pub'] = array(
            '#type' => 'fieldset',
            '#title' => 'Pub',
            '#collapisble' => TRUE,
            '#collapsed' => FALSE,
        );

        $page_limit = \Drupal::state()->get('chado_curator_pub_page_limit', 10);
        $form['pub']['page_limit'] = array(
            '#type' => 'textfield',
            '#title' => 'Page limit',
            '#description' => 'The number of rows to show on each page. (Default=10)',
            '#default_value' => $page_limit,
            '#maxlength' => 3,
            '#size' => 10
        );

        $upload_limit = \Drupal::state()->get('chado_curator_pub_upload_limit', 5);
        $form['pub']['upload_limit'] = array(
            '#type' => 'textfield',
            '#title' => 'Upload limit',
            '#description' => 'The number of files allowed to be uploaded for each pub. (Default=5)',
            '#default_value' => $upload_limit,
            '#maxlength' => 2,
            '#size' => 10
        );

        $fforward_pages = \Drupal::state()->get('chado_curator_pub_fforward_pages', 50);
        $form['pub']['fforward_pages'] = array(
            '#type' => 'textfield',
            '#title' => 'Fast Forward Pages',
            '#description' => 'The number of pages to fast forward when clicking on the >> button. (Default=50)',
            '#default_value' => $fforward_pages,
            '#maxlength' => 3,
            '#size' => 10
        );

        $pub_id_limit = \Drupal::state()->get('chado_curator_pub_pub_id_limit', 0);
        $form['pub']['pub_id_limit'] = array(
            '#type' => 'textfield',
            '#title' => 'Pub ID cutoff',
            '#description' => 'Show only pubs with pub_id greater than or equal to this number. Pubs older than this limit will be hidden from display. (Default=0)',
            '#default_value' => $pub_id_limit,
            '#maxlength' => 128,
            '#size' => 10
        );

        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => 'Save'
        );
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

    }

    /**
    * {@inheritdoc}
    */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();
        if (is_numeric($values['page_limit'])) {
          \Drupal::state()->set('chado_curator_pub_page_limit', (int) $values['page_limit']);
        }
        if (is_numeric($values['upload_limit'])) {
          \Drupal::state()->set('chado_curator_pub_upload_limit', (int) $values['upload_limit']);
        }
        if (is_numeric($values['fforward_pages'])) {
          \Drupal::state()->set('chado_curator_pub_fforward_pages', (int) $values['fforward_pages']);
        }
        if (is_numeric($values['pub_id_limit'])) {
          \Drupal::state()->set('chado_curator_pub_pub_id_limit', (int) $values['pub_id_limit']);
        }
    }
    

}