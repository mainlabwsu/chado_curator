<?php
namespace Drupal\chado_curator\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\file\Entity\File;

class PubForm extends FormBase {

    /**
     * {@inheritdoc}
     */
     public function getFormId() {
        return 'chado_curator_pub_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state) {

        $default_cut = $form_state->getValue('ignore_pid_cutoff') ? $form_state->getValue('ignore_pid_cutoff') : 0;
        $default_pid = $form_state->getValue('pub_id') && trim($form_state->getValue('pub_id')) != '' && is_numeric($form_state->getValue('pub_id'))? (int) $form_state->getValue('pub_id') : NULL;
        $default_title = $form_state->getValue('title') && trim($form_state->getValue('title')) != '' ? $form_state->getValue('title') : NULL;

        $form = array();
        $curators = chado_curator_get_curators();
        $limit = chado_curator_pub_conf_page_limit();
        $num_files = chado_curator_pub_conf_upload_limit();

        // Display assignment
        $assignment = $this->formAssignment($form, $form_state);

        // Display scope
        $scope = $this->formScope($form, $form_state);

        // Display order
        $order = $this->formOrder($form, $form_state);

        // Search
        $this->formSearch($form, $form_state, $default_pid, $default_title, $default_cut);

        // Set up pager
        $pager = $this->formPager($form, $form_state, $limit, $assignment, $scope, $default_pid, $default_title, $default_cut);
        $num_pubs = $pager['num_pubs'];
        $total_pages = $pager['total_pages'];
        $page = $pager['page'];

        // Set up page to list pubs
        $pubs = chado_curator_pub_get_pub_list($limit, $page , $assignment, $scope, FALSE, $order, $default_pid, '=', $default_title, $default_cut);
        if ($num_pubs == 0) {
            $this->formNoPub($form, $form_state, $assignment, $curators, $scope);
        } else {
            // Counter
            $form['counter'] = array('#markup' => '<div id="chado_curator-pub_counter">' . $num_pubs . ' pubs</div>');

            // Pager
            $pages = array();
            for ($i = 0 ; $i < $total_pages; $i ++) {
                $pages[$i] = $i +1;
            }
            $form['fastforward'] = array (
                '#type' => 'button',
                '#name' => 'chado_curator-pub_fastforward',
                '#value' => '>>',
                '#prefix' => "<div id=\"chado_curator-pub_list_fastforward\">",
                '#suffix' => "</div>",
                '#ajax' => array(
                    'callback' => '::ajaxForm',
                    'wrapper' => 'chado_curator_pub_form',
                    'effect' => 'fade'
                )
            );
            $form['next'] = array (
                '#type' => 'button',
                '#name' => 'chado_curator-pub_next',
                '#value' => 'Next',
                '#prefix' => "<div id=\"chado_curator-pub_list_next\">",
                '#suffix' => "</div>",
                '#ajax' => array(
                    'callback' => '::ajaxForm',
                    'wrapper' => 'chado_curator_pub_form',
                    'effect' => 'fade'
                )
            );
            $options = array();
            for ($j = 0; $j < $total_pages; $j ++) {
                $options[$j] = $j +1;
            }
            $form['pager'] = array(
                '#type' => 'hidden',
                '#value' => $page,
                '#prefix' => "<div id=\"chado_curator-pub_list_pager\">Page " . ($page + 1) . " of $total_pages",
                '#suffix' => "</div>",
            );
            $form['previous'] =array (
                '#type' => 'button',
                '#name' => 'chado_curator-pub_previous',
                '#value' => 'Previous',
                '#prefix' => "<div id=\"chado_curator-pub_list_previous\">",
                '#suffix' => "</div>",
                '#ajax' => array(
                    'callback' => '::ajaxForm',
                    'wrapper' => 'chado_curator_pub_form',
                    'effect' => 'fade'
                )
            );
            $form['fastbackward'] =array (
                '#type' => 'button',
                '#name' => 'chado_curator-pub_fastbackward',
                '#value' => '<<',
                '#prefix' => "<div id=\"chado_curator-pub_list_fastbackward\">",
                '#suffix' => "</div>",
                '#ajax' => array(
                    'callback' => '::ajaxForm',
                    'wrapper' => 'chado_curator_pub_form',
                    'effect' => 'fade'
                )
            );

            if ($page == 0) {
                $form['previous']['#disabled'] = TRUE;
                $form['fastbackward']['#disabled'] = TRUE;
            }
            if ($page == $total_pages - 1) {
                $form['next']['#disabled'] = TRUE;
                $form['fastforward']['#disabled'] = TRUE;
            }

            // Reset and Update all
            $form['update_all'] = array(
                '#type' => 'button',
                '#name' => 'chado_curator-pub_update_all',
                '#value' => 'Update All',
                '#prefix' => "<div id=\"chado_curator-pub_list_update_all\">",
                '#suffix' => "</div>",
                '#ajax' => array(
                    'callback' => '::ajaxForm',
                    'wrapper' => 'chado_curator_pub_form',
                    'effect' => 'fade'
                )
            );
            $form['reset'] = array(
                '#type' => 'button',
                '#value' => 'Reset',
                '#name' => 'chado_curator-pub_reset',
                '#prefix' => "<div id=\"chado_curator-pub_list_reset\">",
                '#suffix' => "</div>",
                //'#attributes' => array('onclick' => "window.location='/chado_curator/pub';return false;"),
                '#ajax' => array(
                    'callback' => '::ajaxForm',
                    'wrapper' => 'chado_curator_pub_form',
                    'effect' => 'fade'
                )
            );

            // Table
            $form['pub_list'] = array(
                '#prefix' =>
                '<div id="chado_curator_pub_list">
                    <table>
                    <tr>
                        <th id="chado_curator_pub-pub_id" nowrap=nowrap>Pub ID</th>
                        <th id="chado_curator_pub-title">Title</th>
                        <th id="chado_curator_pub-database">Database</th>
                        <th id="chado_curator_pub-delete">Delete</th>
                        <th id="chado_curator_pub-curate">Curate</th>
                        <th id="chado_curator_pub-curator">Assigned curator</th>
                        <th id="chado_curator_pub-status">Status</th>
                        <th id="chado_curator_pub-updated">Last updated</th>
                        <th id="chado_curator_pub-action">Action</th>
                    </tr>
                    ',
                '#suffix' => '</table></div>',
                '#type' => 'container',
                '#tree' => TRUE
            );

            // Rows
            $row_counter = 0;
            foreach ($pubs AS $i => $pub) {
                $class = $row_counter % 2 == 0? 'even' : 'odd';
                $row_counter ++;
                $link = chado_display_get_path() . '/pub/' . $pub->pid;
                // Cells
                $form['pub_list'][$i]['pub_id'] = array(
                    '#id' => 'chado_curator-' . $i . '-pub_id',
                    '#type' => 'hidden',
                    '#value' => $pub->pid,
                    '#prefix' => '<tr class="chado_curator_pub-' . $class . '_row"><td><div id="chado_curator_pub_item-pub_id" class="chado_curator_pub_form_item">' . $pub->pid,
                    '#suffix' => '</div></td>'
                );
                $form['pub_list'][$i]['title'] = array(
                    '#id' => 'chado_curator-' . $i . '-title',
                    '#prefix' => '<td><div id="chado_curator_pub_item-title" class="chado_curator_pub_form_item">',
                    '#suffix' => '</div></td>'
                );
                if ($link) {
                    $form['pub_list'][$i]['title']['#markup'] = "<a href=$link target=_blank>" . $pub->title . '</a>';
                }
                else {
                    $form['pub_list'][$i]['title']['#markup'] = $pub->title;
                }
                $form['pub_list'][$i]['db'] = array(
                    '#id' => 'chado_curator-' . $i . '-db',
                    '#markup' => $pub->db,
                    '#prefix' => '<td><div id="chado_curator_pub_item-db" class="chado_curator_pub_form_item">',
                    '#suffix' => '</div></td>'
                );
                $form['pub_list'][$i]['delete'] = array(
                    '#id' => 'chado_curator-' . $i . '-delete',
                    '#type' => 'checkbox',
                    '#default_value' => $pub->delete,
                    '#prefix' => '<td><div id="chado_curator_pub_item-delete" class="chado_curator_pub_form_item">',
                    '#suffix' => '</div></td>'
                );
                $op_curate = chado_curator_pub_conf_curate ();
                $form['pub_list'][$i]['curate'] = array(
                    '#id' => 'chado_curator-' . $i . '-curate',
                    '#type' => 'radios',
                    '#options' => $op_curate,
                    '#default_value' => $pub->curate,
                    '#prefix' => '<td><div id="chado_curator_pub_item-curate" class="chado_curator_pub_form_item">',
                    '#suffix' => '</div></td>'
                );
                $curators = chado_curator_get_curators('na', NULL);
                $form['pub_list'][$i]['assigned_uid'] = array(
                    '#id' => 'chado_curator-' . $i . '-assigned_uid',
                    '#type' => 'select',
                    '#options' => $curators,
                    '#default_value' => $pub->assigned_uid,
                    '#prefix' => '<td><div id="chado_curator_pub_item-curator" class="chado_curator_pub_form_item">',
                    '#suffix' => '</div></td>'
                );
                $op_status = chado_curator_pub_conf_status();
                $form['pub_list'][$i]['status'] = array(
                    '#id' => 'chado_curator-' . $i . '-status',
                    '#type' => 'select',
                    '#options' => $op_status,
                    '#default_value' => $pub->status,
                    '#prefix' => '<td><div id="chado_curator_pub_item-status" class="chado_curator_pub_form_item">',
                    '#suffix' => '</div></td>'
                );
                $form['pub_list'][$i]['lastupdated'] = array(
                    '#id' => 'chado_curator-' . $i . '-lastupdated',
                    '#markup' => $pub->timestamp? date("Y-m-d h:i",$pub->timestamp) : 'na',
                    '#prefix' => '<td><div id="chado_curator_pub_item-updated" class="chado_curator_pub_form_item">',
                    '#suffix' => '</div></td>'
                );
                $form['pub_list'][$i]['action'] = array(
                '#id' => 'chado_curator-' . $i . '-action',
                '#name' => "chado_curator-update_$i",
                '#type' => 'button',
                '#value' => "Update",
                '#ajax' => array(
                    'callback' => '::ajaxForm',
                    'wrapper' => 'chado_curator_pub_form',
                    'effect' => 'fade'
                ),
                '#prefix' => '<td><div id="chado_curator_pub_item-action" class="chado_curator_pub_form_item">',
                '#suffix' => '</div></td></tr>'
                );

                $form['pub_list'][$i]['detail'] = array(
                    '#title' => '+',
                    '#type' => 'details',
                    '#collapsible' => TRUE,
                    '#collapsed' => TRUE,
                    '#prefix' => '<tr class="chado_curator_pub-details chado_curator_pub-' . $class . '_row"><td colspan=9>',
                    '#suffix' => '</td></tr>',
                );
                $form['pub_list'][$i]['detail']['files'] = array(
                    '#type' => 'container',
                    '#prefix' => '<div id="chado_curator_pub_detail_row1"><div id="chado_curator_pub_item-files" class="chado_curator_pub_form_detail_items"><div><strong>Files</strong></div>',
                    '#suffix' => '</div>',
                );
                $files = $pub->file_path ? json_decode($pub->file_path) : [];
                $fpath = [];
                $position = 0;
                foreach ($files AS $fid) {
                    if ($fid) {
                        $fp = File::load($fid);
                        if (is_object($fp)) {
                            $fpath[$position] = $fp;
                            $position ++;
                        }
                    }
                }

                for ($j = 0; $j < $num_files; $j ++) {
                    // Show file for deletion if upload exists
                    if (key_exists($j, $fpath)) {
                        $form['pub_list'][$i]['detail']['files'][$fpath[$j]->id()]['file_delete'] = array(
                            '#id' => 'chado_curator-' . $i . '-file_delete_' . $j,
                            '#type' => 'checkbox',
                            '#title' => 'Delete',
                            '#description' => '<a href=' . \Drupal::service('file_url_generator')->generateAbsoluteString($fpath[$j]->getFileUri()) . '>' . $fpath[$j]->getFileName() . '</a>'
                        );
                    }
                    // Otherwise, show upload widget
                    else {
                        $def = isset($files[$j]) ? (int) $files[$j] : NULL;
                        $form['pub_list'][$i]['detail']['files'][$j]['file_path'] = array(
                            '#id' => 'chado_curator-' . $i . '-file_path_' . $j,
                            '#name' => 'chado_curator-' . $i . '-file_path_' . $j,
                            '#type' => 'file'
                        );
                    }
                }

                $form['pub_list'][$i]['detail']['comments'] = array(
                    '#id' => 'chado_curator-' . $i . '-comments',
                    '#title' => 'Comments',
                    '#type' => 'textarea',
                    '#cols' => 10,
                    '#rows' => 5,
                    '#default_value' => $pub->comments,
                    '#prefix' => '<div id="chado_curator_pub_item-comments" class="chado_curator_pub_form_detail_items">',
                    '#suffix' => '</div>',
                );

                $history = chado_curator_get_history($pub->history);
                $form['pub_list'][$i]['detail']['history'] = array(
                    '#id' => 'chado_curator-' . $i . '-history',
                    '#markup' => '<strong>History</strong>' . $history,
                    '#type' => 'markup',
                    '#prefix' => '<div id="chado_curator_pub_item-history" class="chado_curator_pub_form_detail_items">',
                    '#suffix' => '</div></div>',
                );

                $summary = chado_curator_get_features($pub->pid);
                $form['pub_list'][$i]['detail']['data_summary'] = array(
                    '#id' => 'chado_curator-' . $i . '-data_summary',
                    '#markup' => '<strong>Final Data Summary</strong>' . $summary,
                    '#type' => 'markup',
                    '#prefix' => '<div id="chado_curator_pub_detail_row2"><div id="chado_curator_pub_item-data_summary" class="chado_curator_pub_form_detail_items">',
                    '#suffix' => '</div>',
                );

                $dup = chado_curator_get_duplicates($pub->pid);
                $has_dup = preg_match('/No duplicate found/', $dup) ? FALSE : TRUE;
                if ($has_dup) {
                $form['pub_list'][$i]['detail']['#title'] = '<div style=color:red>+ (duplicate found)</div>';
                }
                $form['pub_list'][$i]['detail']['duplicates'] = array(
                    '#id' => 'chado_curator-' . $i . '-duplicates',
                    '#markup' => '<strong>Possible Duplicates</strong>' . $dup,
                    '#type' => 'markup',
                    '#prefix' => '<div id="chado_curator_pub_item-duplicates" class="chado_curator_pub_form_detail_items">',
                    '#suffix' => '</div>',
                );

                $future = $pub->future ? json_decode($pub->future) : '';
                $form['pub_list'][$i]['detail']['future'] = array(
                    '#id' => 'chado_curator-' . $i . '-future',
                    '#title' => 'Future Curation',
                    '#type' => 'checkboxes',
                    '#default_value' => $future ? $future : array(),
                    '#options' => chado_curator_pub_conf_future(),
                    '#prefix' => '<div id="chado_curator_pub_item-future" class="chado_curator_pub_form_detail_items">',
                    '#suffix' => '</div></div>',
                );
            }

            // Duplicate buttons
            $form['fastforward2'] = array (
                '#type' => 'button',
                '#name' => 'chado_curator-pub_fastforward',
                '#value' => '>>',
                '#prefix' => "<div id=\"chado_curator-pub_list_fastforward\">",
                '#suffix' => "</div>",
                '#ajax' => array(
                    'callback' => '::ajaxForm',
                    'wrapper' => 'chado_curator_pub_form',
                    'effect' => 'fade'
                )
            );
            $form['next2'] = array (
                '#type' => 'button',
                '#name' => 'chado_curator-pub_next',
                '#value' => 'Next',
                '#prefix' => "<div id=\"chado_curator-pub_list_next\">",
                '#suffix' => "</div>",
                '#ajax' => array(
                    'callback' => '::ajaxForm',
                    'wrapper' => 'chado_curator_pub_form',
                    'effect' => 'fade'
                )
            );
            $form['pager2'] = array(
                '#type' => 'hidden',
                '#value' => $page,
                '#prefix' => "<div id=\"chado_curator-pub_list_pager\">Page " . ($page + 1) . " of $total_pages",
                '#suffix' => "</div>",
            );
            $form['previous2'] =array (
                '#type' => 'button',
                '#name' => 'chado_curator-pub_previous',
                '#value' => 'Previous',
                '#prefix' => "<div id=\"chado_curator-pub_list_previous\">",
                '#suffix' => "</div>",
                '#ajax' => array(
                    'callback' => '::ajaxForm',
                    'wrapper' => 'chado_curator_pub_form',
                    'effect' => 'fade'
                )
            );
            $form['fastbackward2'] =array (
                '#type' => 'button',
                '#name' => 'chado_curator-pub_fastbackward',
                '#value' => '<<',
                '#prefix' => "<div id=\"chado_curator-pub_list_fastbackward\">",
                '#suffix' => "</div>",
                '#ajax' => array(
                    'callback' => '::ajaxForm',
                    'wrapper' => 'chado_curator_pub_form',
                    'effect' => 'fade'
                )
            );

            if ($page == 0) {
                $form['previous2']['#disabled'] = TRUE;
                $form['fastbackward2']['#disabled'] = TRUE;
            }
            if ($page == $total_pages - 1) {
                $form['next2']['#disabled'] = TRUE;
                $form['fastforward2']['#disabled'] = TRUE;
            }

            // Reset and Update all
            $form['update_all2'] = array(
                '#type' => 'button',
                '#name' => 'chado_curator-pub_update_all',
                '#value' => 'Update All',
                '#prefix' => "<div id=\"chado_curator-pub_list_update_all\">",
                '#suffix' => "</div>",
                '#ajax' => array(
                    'callback' => '::ajaxForm',
                    'wrapper' => 'chado_curator_pub_form',
                    'effect' => 'fade'
                )
            );
            $form['reset2'] = array(
                '#type' => 'button',
                '#value' => 'Reset',
                '#name' => 'chado_curator-pub_reset',
                '#prefix' => "<div id=\"chado_curator-pub_list_reset\">",
                '#suffix' => "</div>",
                //'#attributes' => array('onclick' => "window.location='/chado_curator/pub';return false;"),
                '#ajax' => array(
                    'callback' => '::ajaxForm',
                    'wrapper' => 'chado_curator_pub_form',
                    'effect' => 'fade'
                )
            );
        }

        $form['#prefix'] = '<div id="chado_curator_pub_form">';
        $Form['#suffix'] = '</div>';
        $form['#attached']['library'] = ['chado_curator/chado_curator'];
        return $form;
    }

    function formAssignment(&$form, &$form_state) {
        $user = \Drupal::currentUser();
        $curators = chado_curator_get_curators();
        $assignment = $form_state->getValue('assignment') != NULL ? $form_state->getValue('assignment') : $user->id();
        $form['assignment'] = array(
            '#id' => 'chado_curator-assignment',
            '#type' => 'select',
            '#title' => t('Assigned to'),
            //'#description' => 'Show only pubs that are assigned to selected user.',
            '#options' => $curators,
            '#default_value' => $user->id(),
            '#prefix' => '<div id="chado_curator-pub_assignment">',
            '#suffix' => '</div>',
            '#ajax' => array(
                'callback' => '::ajaxForm',
                'wrapper' => 'chado_curator_pub_form',
                'effect' => 'fade'
            )
        );
        return $assignment;
    }

    function formScope(&$form, &$form_state) {
        $scope = $form_state->getValue('scope') ? $form_state->getValue('scope') : 'WORK';
        $form['scope'] = array(
            '#id' => 'chado_curator-scope',
            '#type' => 'radios',
            '#title' => t('Scope'),
            /*'#description' => t(
                'Show pubs that are within selected scope. Default to show the \'Working List\'
                (i.e. Pubs not flagged \'No Curate/Future\' or \'Delete\'. Status not equals \'Data published\')'),*/
            '#options' => array(
                'WORK' => 'Working List',
                'DELETE' => 'Pubs flagged \'Delete\'',
                'CURATE' => 'Pubs flagged \'Curate\'',
                'CURATEINC' => 'Pubs flagged \'Curate\' (include old pubs)',
                'FUTURE' => 'Pubs flagged for \'Future\' curation',
                'READY' => 'Status = Ready to check',
                'CHECKED' => 'Status = Data checked',
                'LOADED' => 'Status = Loaded to Chado',
                'PUBLISHED' => 'Status = Data published',
                'ALL' => 'All pubs',
            ),
            '#default_value' => 'WORK',
            '#prefix' => '<div id="chado_curator-pub_scope">',
            '#suffix' => '</div>',
            '#ajax' => array(
                'callback' => '::ajaxForm',
                'wrapper' => 'chado_curator_pub_form',
                'effect' => 'fade'
            )
        );
        return $scope;
    }

    function formOrder(&$form, &$form_state) {
        $order = $form_state->getValue('order') ? $form_state->getValue('order') : 'DESC';
        $form['order'] = array(
            '#id' => 'chado_curator-order',
            '#type' => 'radios',
            '#title' => t('Order'),
            '#options' => array('DESC' => 'New pubs first', 'ASC' => 'Old pubs first', 'TITLE_ASC' => 'Alphabetically by title (Ascending)', 'TITLE_DESC' => 'Alphabetically by title (Descending)'),
            '#default_value' => 'DESC',
            '#prefix' => '<div id="chado_curator-pub_order">',
            '#suffix' => '</div>',
            '#ajax' => array(
                'callback' => '::ajaxForm',
                'wrapper' => 'chado_curator_pub_form',
                'effect' => 'fade'
            )
        );
        return $order;
    }

    function formSearch (&$form, &$form_state, $default_pid, $default_title, $default_cut) {
        $form['criteria'] = array(
            '#type' => 'fieldset',
            '#title' => 'Search Pub',
            '#collapsible' => TRUE,
            '#collapsed' => FALSE,
            '#prefix' => '<div id=chado_curator-pub_criteria>',
            '#suffix' => '</div>'
        );
        $form['criteria']['ignore_pid_cutoff'] = array(
            '#type' => 'checkbox',
            '#title' => 'Include all hidden pubs',
            //'#description' => 'Search all pubs including those hidden by the Assignment or Scope selection, or by the \'Pub ID\' cutoff setting.',
            '#default_value' => $default_cut,
        );
        $form['criteria']['pub_id'] = array(
            '#type' => 'textfield',
            '#title' => 'Pub ID',
            '#size' => 10,
            '#maxlength' => 20,
            '#default_value' => $default_pid
        );
        $form['criteria']['title'] = array(
            '#type' => 'textfield',
            '#title' => 'Keyword in Title',
            '#default_value' => $default_title
        );
        $form['criteria']['search'] = array (
            '#id' => 'chado_curator-search',
            '#type' => 'button',
            '#name' => 'chado_curator-pub_search',
            '#value' => 'Search',
            '#prefix' => "<div id=\"chado_curator-pub_list_search\">",
            '#suffix' => "</div>",
            '#ajax' => array(
                'callback' => '::ajaxForm',
                'wrapper' => 'chado_curator_pub_form',
                'effect' => 'fade'
            )
        );
    }

    function formPager(&$form, &$form_state, $limit, $assignment, $scope, $pid, $title, $ignore_pid_cutoff) {
        $jump = chado_curator_pub_conf_fforward_pages();
        $count_pubs = chado_curator_pub_get_pub_list(NULL, 0 , $assignment, $scope, TRUE, NULL, $pid, '=', $title, $ignore_pid_cutoff);
        $num_pubs = $count_pubs[0]->count;
    
        $total_pages = (int) ($num_pubs / $limit);
        if ($num_pubs % $limit != 0) {
            $total_pages ++;
        }
        $page = $form_state->getValue('pager') ? $form_state->getValue('pager') : 0;
    
        if($form_state->getTriggeringElement()){
            $trig_ele = $form_state->getTriggeringElement();
            if ($trig_ele['#type'] == 'button') {
                $action = $trig_ele['#value'];
                if ($action == 'Next' && $page + 1 < $total_pages) {
                    $page ++;
                } else if ($action == 'Previous' && $page - 1 >= 0) {
                    $page --;
                } else if ($action == '>>') {
                    $to_page = $page + $jump;
                    if ($to_page < $total_pages) {
                        $page = $to_page;
                    } else {
                        $page = $total_pages - 1;
                    }
                } else if ($action == '<<') {
                    $to_page = $page - $jump;
                    if ($to_page < 0) {
                        $page = 0;
                    } else {
                        $page = $to_page;
                    }
                }
            } else if ($trig_ele['#type'] == 'radio') {
                $page = 0;
            }
        }
        return array('num_pubs' => $num_pubs, 'total_pages' => $total_pages, 'page' => $page);
    }

    function formNoPub(&$form, &$form_state, $assignment, $curators, $scope) {
        $mesg = 'No pub available.';
        if (!is_numeric($assignment) || $assignment > 0) {
            if ($scope == 'ALL') {
                $mesg = 'No pub assigned to: ' . $curators[$assignment];
            }
        }
        $form['message'] = array(
            '#id' => 'chado_curator-message',
            '#markup' => t($mesg),
            '#prefix' => '<div style="clear:both">',
            '#suffix' => '</div>'
        );
    }




    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        if (trim($form_state->getValue('pub_id')) && !is_numeric($form_state->getValue('pub_id'))) {
            $form_state->setErrorByName('pub_id', 'Pub ID should be an integer');
        }
    
        // Clear the table if this is an AJAX call and not 'Save'
        if($form_state->getTriggeringElement()){
            $trig_ele = $form_state->getTriggeringElement();
            // Save all pubs
            if ($trig_ele['#name'] == 'chado_curator-pub_update_all') {
                $pubs = $form_state->getValue('pub_list');
                foreach ($pubs AS $i => $pub) {
                    $pass = $this->checkCuratorStatus($pub);
                    if (!$pass) {
                        $form_state->setErrorByName("pub_list][$i][status", 'Status cannot be changed without an assigned curator.');
                    }
                    else {
                        chado_curator_pub_insert_or_update_pub ($pub);
                    }
                }
            }
            // Update only single pub
            if (preg_match('/^chado_curator-(\d+)-action$/', $trig_ele['#id'], $matches)) {
                $i = $matches[1];
                $publist = $form_state->getValue('pub_list');
                $pub = $publist[$i];
                $pass = $this->checkCuratorStatus($pub);
                if (!$pass) {
                    $form_state->setErrorByName("pub_list][$i][status", 'Status cannot be changed without an assigned curator.');
                }
                else {
                    chado_curator_pub_insert_or_update_pub ($pub);
                }
            }
    
            if($trig_ele['#id'] == 'chado_curator-search') {
                $form_state->unsetValue('pager');
            }
            // Clear the pager if switching assigned curator
            if($trig_ele['#id'] == 'chado_curator-assignment') {
                $form_state->unsetValue('pager');
                //$form_state->setInput('pager');
                $form_state->unsetValue('scope');
                //$form_state->unsetInput('scope');
                $form_state->unsetValue('ignore_pid_cutoff');
                //$form_state->unsetInput('ignore_pid_cutoff');
                $form_state->unsetValue('pub_id');
                //$form_state->unsetInput('pub_id');
                $form_state->unsetValue('title');
                //$form_state->unsetInput('title');
            }
            if($trig_ele['#name'] == 'scope') {
                $form_state->unsetValue('ignore_pid_cutoff');
                //$form_state->unsetInput('ignore_pid_cutoff');
                $form_state->unsetValue('pub_id');
                //$form_state->unsetInput('pub_id');
                $form_state->unsetValue('title');
                //$form_state->unsetInput('title');
            }
        }
    }

    function checkCuratorStatus (&$pub) {
        if ($pub['assigned_uid'] == 0 && $pub['status']) {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    /**
    * {@inheritdoc}
    */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        $values = $form_state->getValues();

        
    }
    
    public function ajaxForm ($form, $form_state) {
        return $form;
    }
}