<?php

namespace Drupal\chado_curator\Commands;

use Drush\Commands\DrushCommands;

/**
 * Drush commands
 */
class ChadoCuratorCommands extends DrushCommands {
  
  /**
   * Migrate ChadoCurator Data
   *
   * @command chado_curator:chado_curator-migrate
   * @aliases curatormigrate
   * @usage curatormigrate --host=localhost --port=5432 --dbname=sourcedb --user=myuser --password=mypass
   *   Migrate ChadoCurator Pub Data from source database
   */
  public function ChadoCuratorMigrate($options = ['host' => 'localhost', 'port' => '5432', 'dbname' => NULL, 'user' => NULL, 'password' => NULL, 'schema' => NULL]) {
    $host = $options ['host'];
    $port = $options ['port'];
    $dbname = $options ['dbname'];
    $user = $options['user'];
    $password = $options['password'];
    
    if ($user && $dbname && $password) {
      $dbconn = pg_connect("host=$host port=$port dbname=$dbname user=$user password=$password");
      $target = \Drupal::database()->getConnectionOptions();
      $target_host = $target['host'];
      $target_port = $target['port'];
      $target_user = $target['username'];
      $target_pass = $target['password'];
      $target_db = $target['database'];
      $target_conn = pg_connect("host=$target_host port=$target_port dbname=$target_db user=$target_user password=$target_pass");
      // COPY chado_curator_pub table
      $sql = "SELECT * FROM chado_curator_pub ORDER BY pub_id";
      $results = pg_query($dbconn, $sql);
      try {
        $counter_i = 0;
        $counter_r = 0;
        while ($obj = pg_fetch_object($results)) {
          $exists = \Drupal::database()->query("SELECT pub_id FROM chado_curator_pub WHERE pub_id = " . $obj->pub_id)->fetchField();
          if (!$exists) {
            $insert = "
            INSERT INTO chado_curator_pub (
                pub_id, 
                delete, 
                curate, 
                assigned_uid, 
                status, 
                timestamp, 
                file_path, 
                comments, 
                history, 
                future
            ) VALUES (" .
                $obj->pub_id . ", " . 
                $obj->delete . ", " . 
                $obj->curate . ", " . 
                $obj->assigned_uid . ", " . 
                $obj->status . ", " . 
                $obj->timestamp . ", '" . 
                $obj->file_path . "', '". 
                str_replace("'", "''", $obj->comments) . "', '" . 
                $obj->history . "', '" . 
                $obj->future . 
            "')";
            pg_query($target_conn, $insert);
            $counter_i ++;
          }
          else {
            $counter_r ++;
          }
        }
        print "$counter_i records migrated.\n";
        if ($counter_r > 0) {
          print "$counter_r records already existed and not migrated.\n";
        }
      } catch (Exception  $e) {
        print $e;
      }
    }
    else {
      print "[Error] Please provide dbname, user, and password of the source database.\n";
    }    
  }
}